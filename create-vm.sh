#!/bin/bash

# https://serverfault.com/questions/697474/installing-vm-on-centos-6-6-with-virt-install-hangs
# try adding 
#  text console=ttyS0

if [ $# -ne 5 ] ; then
	echo "Usage: $0 <OS> <NAME> <RAM_GB> <DISK_GB> <CPUs>"
	echo "  Where OS is one of the following"
	echo "    centos-6.5-64"
	echo "    centos7"
	echo "    fedora-19-64"
	echo "    ubuntu-12.04-64"
	echo "    debian-7.1-64"
	exit 1
fi

# http://ostolc.org/kvm-guest-creation-and-automated-installation.html
#  virt-install --name test123 --memory 2048 --vcpus 2 --disk size=30 --location /root/CentOS-7-x86_64-Minimal-1810.iso --initrd-inject /root/Templates/test123.cfg

# Example - ./create-vm.sh centos-6.5-64 server1 8 100 4
# Example - ./create-vm.sh centos7 test123 8 100 4
# Example - ./create-vm.sh fedora-19-64 server2 4 100 2
# Example - ./create-vm.sh ubuntu-12.04-64 server3 4 80 2
# Example - ./create-vm.sh debian-7.1-64 server4 4 80 4

OS=$1
NAME=$2
RAM_MB=$(($3*1024))
DISK_GB=$4
VCPUS=$5

# http://mirror.spreitzer.ch/centos/7.6.1810/isos/x86_64/CentOS-7-x86_64-Minimal-1810.iso

if [ "$OS" == "centos-6.5-64" ] ; then
        LOCATION="http://mirror.i3d.net/pub/centos/6.5/os/x86_64/"
	EXTRA_ARGS="ks=http://ostolc.org/pxe/centos-6.5-64.cfg"
elif [ "$OS" == "centos7" ] ; then
        LOCATION=/root/CentOS-7-x86_64-Minimal-1810.iso
#        EXTRA_ARGS=/root/Templates/defualt.cfg
        EXTRA_ARGS=/root/Templates/test123.cfg
elif [ "$OS" == "fedora-19-64" ] ; then
        LOCATION="http://mirror.i3d.net/pub/fedora/linux/releases/19/Fedora/x86_64/os/"
        EXTRA_ARGS="ks=http://ostolc.org/pxe/fedora-19-64.cfg"
elif [ "$OS" == "ubuntu-12.04-64" ] ; then
        LOCATION="http://archive.ubuntu.com/ubuntu/dists/precise/main/installer-amd64/"
        EXTRA_ARGS="auto=true interface=eth0 hostname=ubuntu1204 url=http://ostolc.org/pxe/ubuntu-12.04-64.cfg"
elif [ "$OS" == "debian-7.1-64" ] ; then
        LOCATION="http://ftp.debian.org/debian/dists/Debian7.1/main/installer-amd64/"
        EXTRA_ARGS="auto=true interface=eth0 hostname=debian71 domain=localdomain url=http://ostolc.org/pxe/debian-7.1-64.cfg"
else
        echo "Wrong OS!"
        exit 1
fi

virt-install                            \
        --connect qemu:///system        \
        --virt-type kvm                 \
	--os-type linux			\
	--os-varian virtio26		\
        --name $NAME                    \
        --ram $RAM_MB                   \
        --disk path=/var/lib/libvirt/images/$NAME.qcow2,format=qcow2,size=$DISK_GB	\
        --network bridge=br0            \
        --vcpus $VCPUS                  \
        --graphics vnc                  \
        --noautoconsole                 \
        --location $LOCATION            \
        --extra-args "$EXTRA_ARGS"
